import {getModelSchemaRef, post, requestBody} from "@loopback/openapi-v3";
import {Email} from "../models";

export class EmailController {
    constructor() {
    }

    @post('/sendEmail', {
        responses: {
            '200': {
                description: 'Send an email',
                content: {'application/json': {schema: getModelSchemaRef(String)}},
            },
        },
    })
    async sendEmail(
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(Email, {title: 'newEmail', exclude: ['id']}),
                },
            },
        })
            e: Email,
    ): Promise<Email> {

        const nodemailer = require("nodemailer");

        let transporter = nodemailer.createTransport({
            host: "smtp.sendgrid.net",
            port: 25,
            auth: {
                user: "apikey",
                pass: process.env.MAIL_AUTH
            }
        });

        let info = await transporter.sendMail({
            from: process.env.MAIL_SDR,
            to: process.env.MAIL_OWN,
            subject: "Mensaje de tu portfolio personal",
            text: "Name: " + e.name + "Address: " + e.emailadr + "Message: " + e.message,
            html: "<b>Name: </b>" + e.name + "<br>" + "<b>Address: </b>" + e.emailadr + "<br>" + "<b>Message: </b>" + e.message,
        });

        console.log("Message sent: %s", info.messageId);

        return e;
    }

}
